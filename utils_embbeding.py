import umap.umap_ as umap
import numpy as np
from sklearn.preprocessing import StandardScaler
import pandas as pd
import pickle
import json
import matplotlib.pyplot as plt


def embeding(params, save=True):
    Cvector = np.load(params['Control_path'])
    Pvector = np.load(params['Parkinson_path'])
    randomState = params['random_state']
    dir_save = params['path_results']

    reducer = umap.UMAP(random_state=randomState)
    vectors = np.vstack((Pvector, Cvector))  #concatena verticalmente
    #print(vectors.shape) Vector de No.Samples x SizeDescriptor. (176,210)
    data = pd.DataFrame(vectors)  #Crea dataframe
    data['label'] = ['Parkinson'] * 88 + ['Control'] * 88  #Pone labels
    dataValues = data[data.columns[data.columns != 'label']].values
    scaled_data = StandardScaler().fit_transform(dataValues)
    embedding = reducer.fit_transform(scaled_data)
    #if save:
        #save_json(dir_save, params)
        #save_pickle(dir_save, embedding)
        #return
    return embedding


def plt_embedding(title, data_path, save=False):
    data = load_pickle(data_path)
    P, NP = data[:88, :], data[
        88:, :]  #se dividen los puntos en puntos de P y NP
    Px, Py = P[:, 0], P[:, 1]  #Se toman las coordenadas de los P
    NPx, NPy = NP[:, 0], NP[:, 1]  #Se toman las coordenadas de los NP
    font_axis = {
        #'family': 'serif',
        #'color': 'black',
        #'weight': 'normal',
        'size': 21,
    }
    plt.figure(figsize=(10, 10))
    if title: plt.title(str(title), fontdict=font_axis)
    plt.scatter(Px,
                Py,
                c='tab:yellow',
                alpha=0.3,
                s=300,
                edgecolors='none',
                linewidth=4,
                label='Parkinson')
    plt.scatter(NPx,
                NPy,
                color='tab:green',
                alpha=0.3,
                s=300,
                edgecolors='none',
                linewidth=4,
                label='No Parkinson')
    plt.axis('off')
    plt.legend(labelspacing=1, prop={'size': 17}, frameon=False)
    plt.savefig(data_path + '.png', dpi=600)
    #plt.savefig(data_path + '.png')
    plt.show()

#def plt_embedding_v2(title, data_path, save=False):
def plt_embedding_v2(title,data, save=False):
    #data = load_pickle(data_path)
    P, NP = data[:88, :], data[
        88:, :]  #se dividen los puntos en puntos de P y NP
    Px, Py = P[:, 0], P[:, 1]  #Se toman las coordenadas de los P
    NPx, NPy = NP[:, 0], NP[:, 1]  #Se toman las coordenadas de los NP
    font_axis = {
        #'family': 'serif',
        #'color': 'black',
        #'weight': 'normal',
        'size': 21,
    }
    plt.figure(figsize=(10, 10))
    if title: plt.title(str(title), fontdict=font_axis)
    plt.scatter(Px,
                Py,
                c='orange',
                alpha=0.8,
                s=300,
                edgecolors='k',
                #markeredgecolor
                linewidth=2,
                label='Parkinson')
    plt.scatter(NPx,
                NPy,
                color='green',
                alpha=0.8,
                s=300,
                edgecolors='k',
                linewidth=2,
                label='No Parkinson')
    plt.axis('off')
    plt.legend(labelspacing=1, prop={'size': 17}, frameon=False)
    #plt.savefig(data_path + '_v2.png', dpi=600)
    #plt.savefig(data_path + '.png')
    plt.show()    

def save_pickle(dirr, var):
    """[Funcion que cuarda una variable usando libreria pickle]

    Args:
        dirr ([str]): [Directorio con nombre como se quiere guardar la variable 'var'. Ejemplo: 'Files/mi_variable']
        var ([]): [Lo que se quiere guardar]

    Returns:
        []: []
    """
    file = open(dirr, 'wb')
    pickle.dump(var, file)  #Guarda var en con dirección: dirr
    file.close()


def load_pickle(dirr):
    """[Funcion que carga una variable usando libreria pickle]

    Args:
        dirr ([str]): [Directorio con nombre del directorio donde esta guardada la variable 'var'. Ejemplo: 'Files/mi_variable']

    Returns:
        var []: [Lo que se haya guardado]
    """
    file = open(dirr, 'rb')
    var = pickle.load(file)  #Carga el pickle de la dirección: dirr
    file.close()
    return var


def save_json(dirr, config):
    dirr = dirr + '.json'
    with open(dirr, 'w') as f:
        json.dump(config, f)


def load_json(dirr):
    with open(dirr, 'r') as f:
        config = json.load(f)
    return config
