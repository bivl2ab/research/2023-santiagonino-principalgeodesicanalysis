import numpy as np
import scipy
import scipy.linalg as sg
from sklearn.datasets import make_spd_matrix
import glob, os
import matplotlib.pyplot as plt
import itertools


def genera_spd(N, size):
    """
    Compute operators to matrices.
    
    Parameters
    ----------
        N: 
                int.
                No. matrices to generate.
        size: 
                int.
                Dimension of the matrices.
        
    Returns
    -------
        L:
                array.
                vector of N SPD matrices of dimension size x size.
                
    Examples
    --------
        >>> B=genera_spd(20, 4)
            return 20 SPD matrices by 4x4
        
    """

    L = []
    for i in range(N):
        L.append(make_spd_matrix(size))
    return np.array(L)


def _matrix_operator(Ci, operator):
    """
    Compute operators to matrices.
    
    Parameters
    ----------
        Ci: 
                array.
                Matrix.
        operator: 
                Operator or function for apply to Ci
        
    Returns
    -------
        f(Ci):
                array.
                Result of applying the operator to the matrix
                
    Examples
    --------
        >>> B=_matrix_operator(A, np.sqrt)
            return the squared root of matrix A.
        
    """
    #if Ci.dtype.char in typecodes['AllFloat'] and not np.isfinite(Ci).all():
    #raise ValueError("Covariance matrices must be positive definite. Add regularization to avoid this error.")
    eigvals, eigvects = scipy.linalg.eigh(Ci, check_finite=False)
    eigvals = np.diag(operator(eigvals))
    Out = np.dot(np.dot(eigvects, eigvals), eigvects.T)
    return Out


def sqrtm(Ci):
    """
    Compute squared root of a matrix.
    
    Parameters
    ----------
        Ci: 
                array.
                Matrix.       
        
    Returns
    -------
        sqrt(Ci):
                array.
                Squared root of the matrix Ci
                
    Examples
    --------
        >>> B=sqrtm(A)
            return the squared root of matrix A.
        
    """
    return _matrix_operator(Ci, np.sqrt)


def invsqrtm(Ci):
    """
    Compute negative squared root of a matrix, i.e, A^{-\frac{1}{2}}.
    
    Parameters
    ----------
        Ci: 
                array.
                Matrix.       
        
    Returns
    -------
        X:
                array.
                Negative squared root of the matrix Ci
                
    Examples
    --------
        >>> B=invsqrtm(A)
            return A^{-\frac{1}{2}}.
        
    """
    isqrt = lambda x: 1. / np.sqrt(x)
    return _matrix_operator(Ci, isqrt)


def logm(Ci):
    """
    Compute logarithm of a matrix.
    
    Parameters
    ----------
        Ci: 
                array.
                Matrix.       
        
    Returns
    -------
        log(Ci):
                array.
                Logarithm of the matrix Ci
                
    Examples
    --------
        >>> B=logm(A)
            return the logarithm of matrix A.
        
    """
    return _matrix_operator(Ci, np.log)


def log_AB(A, B):
    """
    Compute logarithm of a matrix A based on B.
    
    Parameters
    ----------
        A: 
                array.
                Matrix. 
        B:
                array.
                Base matrix.
        
    Returns
    -------
        log_B(A):
                array.
                Logarithm of the matrix A based on B.
    Example
    -------
    """
    B_12 = sqrtm(B)
    B_m12 = invsqrtm(B)
    L = np.dot(np.dot(B_m12, A), B_m12)
    L_log = sg.logm(L)
    out = np.dot(np.dot(B_12, L_log), B_12)
    return np.array(out)


def expm(Ci):
    """
    Compute exponential of a matrix.
    
    Parameters
    ----------
        Ci: 
                array.
                Matrix.       
        
    Returns
    -------
        exp(Ci):
                array.
                Exponential of the matrix Ci
                
    Examples
    --------
        >>> B=expm(A)
            return the exponential of matrix A.
        
    """
    return _matrix_operator(Ci, np.exp)


def exp_AB(A, B):  #A y B tipo arrays
    B_12 = sqrtm(B)
    B_m12 = invsqrtm(B)

    L = np.dot(np.dot(B_m12, A), B_m12)

    L_exp = sg.expm(L)  #Calculo de log(B^(-1/2) A B^(-1/2))

    out = np.dot(np.dot(B_12, L_exp), B_12)
    return out


def media_error(x_media):
    """[Calcula el error de la media como la suma de los cuadrados de los autovalores]

    Args:
        x_media ([array]): [Covariance matrix dim: s x s]

    Returns:
        [type]: [description]
    """    #s=sg.eigh(x_media,check_finite=False,eigvals_only=True)
    s = np.linalg.eigvals(x_media)
    for i in range(len(s)):
        if (s[i] <= 0):
            s[i] = 1
    x_abs = np.sum(np.log(s**2))
    return x_abs


def prom_log(P, m):  #promedio de los logaritmos en base m
    suma = 0
    k = 0
    for i in P:
        k = k + 1
        #print("{it} logaritmo: {log}".format(it=k,log= log_AB(i,m)))
        suma = suma + log_AB(i, m)
    prom = suma / len(P)
    return prom


def exp_eg(X, B):
    eigvals, eigvects = scipy.linalg.eigh(X, B)
    eigvals = np.diag(np.exp(eigvals))
    out = np.dot(np.dot(np.dot(np.dot(B, eigvects), eigvals), eigvects.T), B)
    return out


def log_eg(X, B):
    eigvals, eigvects = scipy.linalg.eigh(X, B)
    eigvals = np.diag(np.log(eigvals))
    out = np.dot(np.dot(np.dot(np.dot(B, eigvects), eigvals), eigvects.T), B)
    return out


def estabilidad(epsilon, nn, sizes, medias):
    """
    Mide la estabilidad de ciertas medias como dif(A,A+epsilon)/epsilon

    Input:
        epsilon   ---   Numero real positivo.
        nn        ---   Numero de matrices del conjunto base (CB).
        sizes     ---   Dimension de las matrices del conjunto base (CB).
        medias    ---   Lista de las medias a las cuales se les quiere medir la estabilidad.
        
    Process..:
        Se genera aleatoriamente CB con "nn" matrices de dim "sizes",
        Se genera un conjunto CE que son las matrices de CB mas una matriz diagonal de "epsilon",
        Con cada media se calcula la estabilidad= (||mean(CB)-mean(CE)||/epsilon)
        
    Observation:
    %% Se puede implementar usando la función de la librería PYRIEMANN: riemann.utils.distance.distance_riemann() 
    """
    conjunto = genera_spd(nn, sizes)
    conjunto_mas_epsilon = []
    epsilon_m = epsilon * np.ones((sizes, sizes))
    for i in range(len(conjunto)):
        conjunto_mas_epsilon.append(epsilon + conjunto[i])
    conjunto_mas_epsilon = np.array(conjunto_mas_epsilon)
    for j in range(len(medias)):
        ini, k = medias[j](conjunto)
        final = medias[j](conjunto_mas_epsilon)[0]
        delta_matrix = ini - final
        delta = np.linalg.norm(delta_matrix, ord='fro')
        b = np.linalg.norm(epsilon_m, ord='fro')
        print('{media}:\n---     {aa}   ---'.format(media=means_str[j],
                                                    aa=delta / epsilon))


def riemannian_distance(A, B):

    #2021_03_31 NO FUNCIONA
    """[Calcula la distancia riemanniana entre las matrices SPD A y B]

    Args:
        A ([array]): [SPD matrix]
        B ([array]): [SPD matrix]

    Returns:
        [float]: [distancia riemanniana entre A y B]
    """
    dist = np.sqrt((np.log(np.linalg.eigvalsh(A, B))**2).sum())
    return dist


def distanceNorm(lista_medias):
    """
    Input:
        medias      ---     lista de medias (Sunpongamos [M_1,...,M_m])
    Process..:
        Se calcula una matriz [dif_ij]_{i=1,j=1}^{m,m} donde dif_ij=||M_i-M_j||
    """
    k = len(lista_medias)
    matrix = np.zeros((k, k))
    for i in range(len(lista_medias)):
        for j in range(len(lista_medias)):
            matrix[i][j] = np.linalg.norm(lista_medias[i][0] -
                                          lista_medias[j][0],
                                          ord='fro')
    return matrix


def to_tangent_space(mats, reff=None):
    """
    Given one or a list of covariance matrices and a reference matrix, maps the covariance(s) matrix(ces) 
    to the tangent plane on the reference matrix.
    
    Parameters
    ----------
        mats:
                    array or list.
                    list of covariance matrices or a single matrix.
                    
        reff:
                    array.
                    reference matrix by default, the identity matrix.
    Returns
    -------
        C:
                    array or list.
                        * One matrix on the tangent plane of 'reff' If introduce a single covariance matrix.
                        * List of matrices on the tangent plane of 'reff' If introduce a list of covariance matrices.
                        
    Example
    -------
    """
    mats = np.array(mats)
    #when is only one list of features
    if len(mats.shape) == 3:
        N, S, S = mats.shape
        if reff is None:
            reff = np.identity(S)
        T = []
        for mat in mats:
            log = log_AB(mat, reff)
            T.append(log)
        return np.array(T)
    elif len(mats.shape) == 2:
        S, S = mats.shape
        if reff is None:
            reff = np.identity(S)
        log = log_AB(mats, reff)
        return np.array(log)
    else:
        return print(
            "ERROR: Only admits list of covariance matrices or a single covariance matrix."
        )


def is_pos_def(x):
    """
    Return if x is a positive definite matrix.
    
    Parameters
    ----------
        mats:
                    array.
                    matrix.
    
     Returns
    -------
                    bool.
                    True if is positive definite
                    
    Examples
    --------
        >>> is_pos_def(A)
            True
    """
    return np.all(np.linalg.eigvals(x) > 0)


def regularization(list_cov, treshold=1e-15):
    """
    Regularize a list of covariance matrices. Find the minimum eigenvalue and if is less or equal cero, sums 1e-9 for all
    matrices.
    
    Parameters
    ----------
        list_cov: 
                    list.
                    List of covariances matrices.  
        treshold:
                    float.
                    The trehold for the minimum eigenvalue, if the minimum eigenvalue is less or equal trehold, then 
                    apply the regularization. 
    Returns
    -------
        X:
                    array.
                    List of regularized covariance matrices list_cov.
                
    Examples
    --------
        >>> A=[C_1,C_2,C_3]
        >>> B=regularization(A)
            return B regularized.
        >>> B=regularization(A,treshold=1e-15)
        
    """

    size = (list_cov[0].shape)[0]
    covs = []

    #Finding the minimum eigenvalue
    eig_min = []
    for i in range(len(list_cov)):
        eig_val = sg.eigh(list_cov[i], check_finite=False, eigvals_only=True)
        em = min(eig_val)
        eig_min.append(em)
    minn = min(eig_min)
    #print(minn)

    #Applying the regularization, depend of treshold
    if minn <= treshold:
        for i in range(len(list_cov)):
            epsilon = np.abs(minn) + 1e-9
            new_cov = list_cov[i] + epsilon * np.eye(size)
            covs.append(new_cov)
        return np.array(covs)
    else:
        return np.array(list_cov)


def min_eigval(covmats):
    """
    Return the minimum eigenvalue.

    Parameters
    ----------
        covmats:
                    array or list.
                    SPD matrix or list of SPD matrices.
    Returns
    -------
        m:
                    float.
                    minimum eigenvalue.
                    
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> min_eigval(A)
    """
    covmats = np.array(covmats)
    if len(covmats.shape) == 2:
        eigvals, eigvects = scipy.linalg.eigh(covmats, check_finite=False)
        m = min(eigvals)
        return m
    elif len(covmats.shape) == 3:
        m_eigvals = []
        for i in covmats:
            eigvals, eigvects = scipy.linalg.eigh(i, check_finite=False)
            m_eigvals.append(min(eigvals))
        m = min(m_eigvals)
        return m
    else:
        return print(
            "ERROR: solo se admite una matriz o una lista de matrices.")


def matrix_norm(mats, typeNorm='frobenius'):
    """
    Return the minimum eigenvalue.

    Parameters
    ----------
        mats:
                    array or list.
                    Matrix or list matrices.
    Returns
    -------
        norm:
                    array or list.
                    Matrix norm or list matrices norms.
                    
    Examples
    --------
        >>> A = genera_spd(5,4)
        >>> matrix_norm(A)
            [4.380021224981749,
             4.699941621926346,
             4.590561180874435,
             4.74805550288807,
             4.679218900102526]
    """
    mats = np.array(mats)

    if len(mats.shape) == 2:
        if typeNorm == 'frobenius':
            norm = np.linalg.norm(mats, ord='fro')
            return norm
        elif typeNorm == 'egvals':
            eigvals, eigvects = scipy.linalg.eigh(mats, check_finite=False)
            norm = np.sqrt(np.sum(eigvals**2))
            return norm
        else:
            return print("Norm not defined")

    elif len(mats.shape) == 3:
        norms = []
        for M in mats:
            if typeNorm == 'frobenius':
                norm = np.linalg.norm(M, ord='fro')
                norms.append(norm)
            elif typeNorm == 'egvals':
                eigvals, eigvects = scipy.linalg.eigh(M, check_finite=False)
                norm = np.sqrt(np.sum(eigvals**2))
                norms.append(norm)
            else:
                return print("Norm not defined")
        return norms
    else:
        return print(
            "ERROR: solo se admite una matriz o una lista de matrices.")

    return


def cov(mats):
    """
    Given one or many list features, compute the covariance matrix or covariances matrices.

    Parameters
    ----------
        mats:
                    array or list.
                    list of features (matrices) or vector with of lists of features.
    Returns
    -------
        C:
                    array or list.
                        * Covariance matrix  If introduce a list of featuresMatrix.
                        * List of covaraince matrices if introduce a vector with of lists of features.
                    
    Examples
    --------
        >>> A=genera_spd(4,520)
        >>> cov(A)
            array([[1.00246976, 0.00992002, 0.00338735, 0.00389759],
            [0.00992002, 1.00253305, 0.00752379, 0.00455961],
            [0.00338735, 0.00752379, 1.00255293, 0.00820412],
            [0.00389759, 0.00455961, 0.00820412, 1.00259009]])
        >>> B=[genera_spd(4,320),genera_spd(4,320),genera_spd(4,320)]
            array([[[1.00404782, 0.00481831, 0.00547179, 0.01394064],
             [0.00481831, 1.00421605, 0.00538248, 0.00505985],
             [0.00547179, 0.00538248, 1.00442593, 0.0044113 ],
             [0.01394064, 0.00505985, 0.0044113 , 1.00431488]],

            [[1.00404499, 0.00659875, 0.00686999, 0.00712339],
             [0.00659875, 1.00406234, 0.00627386, 0.00423979],
             [0.00686999, 0.00627386, 1.00394948, 0.00389156],
             [0.00712339, 0.00423979, 0.00389156, 1.00406607]],

            [[1.00428728, 0.00364867, 0.00420123, 0.00399623],
             [0.00364867, 1.00377857, 0.01235465, 0.00533315],
             [0.00420123, 0.01235465, 1.00439389, 0.00555487],
             [0.00399623, 0.00533315, 0.00555487, 1.00386572]]])
    """
    mats = np.array(mats)
    #when is only one list of features
    if len(mats.shape) == 3:
        F, W, H = mats.shape
        flatten_features = []
        for feature in mats:
            flat_feature = feature.reshape(W * H)
            flat_feature = np.array(flat_feature)
            flatten_features.append(flat_feature)
        C = np.cov(flatten_features)
        return np.array(C)
    #when is a vector with lists of features
    elif len(mats.shape) == 4:
        NF, F, W, H = mats.shape
        C = []
        for list_of_features in mats:
            flatten_features = []
            for feature in list_of_features:
                flat_feature = feature.reshape(W * H)
                flat_feature = np.array(flat_feature)
                flatten_features.append(flat_feature)
            covariance_of_list = np.cov(flatten_features)
            covariance_of_list = np.array(covariance_of_list)
            C.append(covariance_of_list)
        return np.array(C)
    else:
        return print(
            "ERROR: Only admits list of features (matrices) or vector with lists of features."
        )
    return


def redimcov(mats, size=2):
    """
    Given a covariance matrix or a list of covariance matrices, 
    resize the covariance matriz based on its eigenvalues and eigenvectors

    Parameters
    ----------
        mats:
                    array or list of arrays.
                    list of arrays (covaraicne matrices) or an array (covariance matrix).
        size:
                    int
                    new size of the covariance matrix, by default equals 2.
    Returns
    -------
        C:
                    array or list of arrays.
                        * New reshaped Covariance matrices If introduce a list of matrices.
                        * New reshape matrix If introduce a covariance matrix
                    
    Examples
    --------
        >>> A=genera_spd(4,520)
        >>> redimcov(A)
            array([[[4.63020614, 2.30265523],
                [2.30265523, 1.8047864 ]],

               [[0.62296417, 0.48968678],
                [0.48968678, 2.53596382]],

               [[2.66015322, 0.94877286],
                [0.94877286, 0.93183806]],

               [[0.63986088, 0.2654274 ],
                [0.2654274 , 0.93614361]]])
    """
    mats = np.array(mats)
    if len(mats.shape) == 2:
        D, V = scipy.linalg.eigh(
            mats)  #eigenvalues and eigenvector, ascent order
        V = V[:, -size:]  #Take greathers eigenvectors (columns of V)
        new_mat = np.dot(np.dot(V.T, np.diag(D)), V)  # New covariance matrix
        return np.array(new_mat)
    elif len(mats.shape) == 3:
        new_mats = []
        No_mat, s, s = mats.shape
        if size > s:
            return 'Size greater then matrix size'
        for mat in mats:
            D, V = scipy.linalg.eigh(
                mat)  #eigenvalues and eigenvector, ascent order
            V = V[:, -size:]
            new_mat = np.dot(np.dot(V.T, np.diag(D)), V)
            new_mats.append(new_mat)
        return np.array(new_mats)
    else:
        return print("ERROR: Only admits list of matrices or a single matrix.")
    return


def cov2excel(dir_xlsx, mats):
    """
    Given a list of covariance matrices of dimension 2x2, save as 3D vectors in a excel file

    Parameters
    ----------
        dir_xlsx:
                    str.
                    directory to save the excel file.
        mats:
                    list.
                    list of covariance matrices.
    Returns
    -------

                    
    Examples
    --------
        >>> A=genera_spd(20,2)
        >>> cov2excel('prueba2.xlsx',A)            
    """
    workbook = xlsxwriter.Workbook(dir_xlsx)
    worksheet = workbook.add_worksheet()
    fil = 0
    col = 0
    for i in mats:
        worksheet.write(fil, col, i[0, 0])
        worksheet.write(fil, col + 1, i[0, 1])
        worksheet.write(fil, col + 2, i[1, 1])
        fil += 1
    workbook.close()
    return


def flatten_upper_triangular(mat):
    """
    Flat the upper triangular part of the matrix

    Parameters
    ----------
        mat:
                    array.
                    Symetric matrix.
     
    Returns
    -------
        C:
                    Array.
                    1-dimensional vector, the flaten upper triangular part of 'mat'.
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_log_euclidean(A)
            array([[ 0.69372602, -0.10535614,  0.01970381, -0.07283615],
           [-0.10535614,  0.93545507,  0.1942331 ,  0.19177842],
           [ 0.01970381,  0.1942331 ,  0.87940517,  0.02746702],
           [-0.07283615,  0.19177842,  0.02746702,  0.76538154]])
    """
    mat = np.array(mat)
    size, size = mat.shape
    flaten_ut = []
    for k in range(size):  # se vuelve vector la parte diagonal superior
        for l in range(k, size):
            flaten_ut.append(mat[k][l])
    return np.array(flaten_ut)


def mean_log_euclidean(covmats):
    """
    Calculate the Log-Euclidean mean of a list of SPD matrices.
    
    Parameters
    ----------
        covmats: 
                    list.
                    List of SPD matrices.  
     
    Returns
    -------
        C:
                    Array.
                    Log-Euclidean mean of covmats.
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_log_euclidean(A)
            array([[ 0.69372602, -0.10535614,  0.01970381, -0.07283615],
           [-0.10535614,  0.93545507,  0.1942331 ,  0.19177842],
           [ 0.01970381,  0.1942331 ,  0.87940517,  0.02746702],
           [-0.07283615,  0.19177842,  0.02746702,  0.76538154]])        
    """
    covmats = regularization(covmats)
    for i in covmats:
        eigvals, eigvects = scipy.linalg.eigh(i, check_finite=False)
        if min(eigvals) < 1e-10:
            return print("ERROR: No todas las matrices del conjunto son SPD")
    Nt, Ne, Ne = covmats.shape
    J = np.zeros((Ne, Ne))
    #Calculating the mean
    for index in range(Nt):
        eigvals, eigvects = scipy.linalg.eigh(covmats[index, :, :],
                                              check_finite=False)
        logm = np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                      eigvects.T)  # log(C^{-1/2}C_iC^{-1/2})
        J += logm  # \sum log(C^{-1/2}C_iC^{-1/2})

    J = (1 / Nt) * J  # 1/N (\sum log(C^{-1/2}C_iC^{-1/2}))
    eigvals, eigvects = scipy.linalg.eigh(J, check_finite=False)
    C = np.dot(np.dot(eigvects, np.diag(np.exp(eigvals))),
               eigvects.T)  # exp(1/N (\sum log(C^{-1/2}C_iC^{-1/2})))
    C = 0.5 * (C + C.T)  #Symmetrization
    k = 1  # No. Iterations
    return np.array(C)


def mean_pyriemann(covmats,
                   tol=10e-9,
                   maxiter=50,
                   init=None,
                   norm='frobenius',
                   itersTaken=False,
                   norm_approximation=False,
                   imprime=False):
    """
    Calculate the Pyriemann mean of a list of SPD matrices. Based in the python library ''pyriemann``.
    
    Parameters
    ----------
        covmats: 
                    list.
                    List of SPD matrices.  
                    
        tol:
                    float.
                    tolerance to stop the algorithm.
                    
        maxiter:
                    int.
                    Maximum number of iterations of the algorithm.
                    
        init:       
                    array.
                    Base matrix to start the algoritm. Default start with ''arithmetic mean``.
                    Others: ''mean``, ''identity``, ''first``, ''log_euclidean``
                    
        norm:
                    str.
                    Type of norm to use. Default ''frobenius``.
                    see.: matrix_norm
                    
        iterTaken:
                    bool.
                    True, if want to know No. Iterations needed by the algorithm.
                    
        norm_approximation:
                    bool.
                    True, to return a list with history of the Norm value Through the algorithm.
                    
        imprime:
                    bool.
                    True, to print information through the code.
     
    Returns
    -------
        C:
                    Array.
                    Pyriemann mean of covmats.
        it_de_salida (optional):
                    int.
                    No. iterations taken to pass the tolerance for first time.
                    
        lista_aproximaciones (optional):
                    list.
                    All approximations norms from 1 to maxiter.
                
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_pyriemann(A)
            array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
           [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
           [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
           [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]])
        >>> mean_pyriemann(A,itersTaken=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             8)
        >>> mean_pyriemann(A,itersTaken=True, norm_approximation=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             [1.2499134146335775,
              0.038028555697487254,
              0.005157315385987814,
              0.0005183919740273995,
                  .
                  .
                  .
              2.5716369454988992e-14,
              2.6616446248399277e-14],
             8)                             
    """
    covmats = regularization(covmats)
    Nt, Ne, Ne = covmats.shape
    if type(init) == str:
        if init == 'mean':
            C = np.mean(covmats, axis=0)
        if init == 'identity':
            C = np.identity(Ne)
        if init == 'first':
            C = covmats[0]
        if init == 'log_euclidean':
            C = mean_log_euclidean(covmats)[0]
    else:
        if init is None:
            C = np.mean(covmats, axis=0)
        else:
            C = init
    k = 0
    nu = 1.0
    tau = np.finfo(np.float64).max
    crit = np.finfo(np.float64).max
    if norm_approximation == False:
        while (crit > tol) and (k < maxiter) and (nu > tol):
            if imprime:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            C12 = sqrtm(C)
            Cm12 = invsqrtm(C)
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]), Cm12)
                J += logm(tmp)
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            h = nu * crit
            C = np.dot(np.dot(C12, expm(nu * J)), C12)
            C = 0.5 * (C + C.T)

            if h < tau:
                nu = 0.95 * nu
                tau = h
            else:
                nu = 0.5 * nu
        it_de_salida = k
        if itersTaken == False:
            return np.array(C)
        else:
            return np.array(C), it_de_salida
    else:
        lista_aproximaciones = []
        flag = False
        it_de_salida = maxiter
        while (k < maxiter):
            k = k + 1
            C12 = sqrtm(C)
            Cm12 = invsqrtm(C)
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]), Cm12)
                J += logm(tmp)
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            lista_aproximaciones.append(crit)
            h = nu * crit
            C = np.dot(np.dot(C12, expm(nu * J)), C12)
            if h < tau:
                nu = 0.95 * nu
                tau = h
            else:
                nu = 0.5 * nu
            if crit < tol and flag == False:
                it_de_salida = k
                flag = True
            C = 0.5 * (C + C.T)
        if itersTaken == True:
            return np.array(C), lista_aproximaciones, it_de_salida
        else:
            return np.array(C), lista_aproximaciones
    return


def mean_pennec(covmats,
                tol=10e-9,
                maxiter=50,
                init='mean',
                norm='egvals',
                itersTaken=False,
                norm_approximation=False,
                imprime=False):
    """
    Calculate the Pennec mean of a list of SPD matrices. Based in the Pennec's algorithm.
    
    Parameters
    ----------
        covmats:
                    list.
                    List of SPD matrices.
                    
        tol:
                    float.
                    tolerance to stop the algorithm.
                    
        maxiter:
                    int.
                    Maximum number of iterations of the algorithm.
                    
        init:
                    array.
                    Base matrix to start the algoritm. Default start with ''mean`` (arithmetic mean).
                    Others: ''mean``, ''identity``, ''first``, ''log_euclidean``, or SPD matrix array.
                    
        norm:
                    str.
                    Type of norm to use. Default: ''egvals`` (sum of square of eigenvalues).
                    Other: ''frobenius``.
                    
        iterTaken:
                    bool.
                    True, if want to know No. Iterations needed by the algorithm.
                    
        norm_approximation:
                    bool.
                    True, to return a list with history of the Norm value Through the algorithm.
                    
        imprime:
                    bool.
                    True, to print information through the code.
     
    Returns
    -------
        C:
                    Array.
                    Pennec's mean of covmats.
        it_de_salida (optional):
                    int.
                    No. iterations taken to pass the tolerance for first time.
                    
        lista_aproximaciones (optional):
                    list.
                    All approximations norms from 1 to maxiter.
                
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_pennec(A)
            array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
           [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
           [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
           [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]])
        >>> mean_pennec(A,itersTaken=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             8)
        >>> mean_pennec(A,itersTaken=True, norm_approximation=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             [1.2499134146335775,
              0.038028555697487254,
              0.005157315385987814,
              0.0005183919740273995,
                  .
                  .
                  .
              2.5716369454988992e-14,
              2.6616446248399277e-14],
             8)                             
    """
    covmats = regularization(covmats)
    Nt, Ne, Ne = covmats.shape
    #Choosing Initial Matrix
    if type(init) == str:
        if init == 'mean':
            C = np.mean(covmats, axis=0)
        if init == 'identity':
            C = np.identity(Ne)
        if init == 'first':
            C = covmats[0]
        if init == 'log_euclidean':
            C = mean_log_euclidean(covmats)[0]
    elif len(init.shape) == 2:
        C = init
    else:
        return print('No chosen Norm matrix')
    Id = np.array(np.identity(Ne))
    k = 0
    crit = np.finfo(np.float64).max
    isqrt = lambda x: 1. / np.sqrt(x)  # x^{-1/2}
    if norm_approximation == False:
        while (crit > tol) and (k < maxiter):
            if imprime is True:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            eigvals, eigvects = scipy.linalg.eigh(
                C)  #Calculo de eigenvalores de C
            C12 = np.dot(np.dot(eigvects, np.diag(np.sqrt(eigvals))),
                         eigvects.T)  # Calculo de C^{1/2}
            Cm12 = np.dot(np.dot(eigvects, np.diag(isqrt(eigvals))),
                          eigvects.T)  # Calculo de C^{-1/2}
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                #print("Eigenvalores matriz {ind}: {emb}".format(ind=index,emb=scipy.linalg.eigh(covmats[index])[0]))
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]),
                             Cm12)  #C^{-1/2}C_iC^{-1/2}
                eigvals, eigvects = scipy.linalg.eigh(tmp)
                logm = np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                              eigvects.T)  #log(C^{-1/2}C_iC^{-1/2})
                J += logm  # \sum log(C^{-1/2}C_iC^{-1/2})
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            eigvals, eigvects = scipy.linalg.eigh(J, check_finite=False)
            C = np.dot(
                np.dot(
                    C12,
                    np.dot(np.dot(eigvects, np.diag(np.exp(eigvals))),
                           eigvects.T)),
                C12)  # C^{1/2}exp(\sum log(C^{-1/2}C_iC^{-1/2}))C^{1/2}
            C = 0.5 * (C + C.T)  #Symmetrization
        it_de_salida = k
        if itersTaken == False:
            return np.array(C)
        else:
            return np.array(C), it_de_salida
    else:
        lista_aproximaciones = []
        flag = False
        while (k < maxiter):
            if imprime is True:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            eigvals, eigvects = scipy.linalg.eigh(
                C)  #Calculo de eigenvalores de C
            C12 = np.dot(np.dot(eigvects, np.diag(np.sqrt(eigvals))),
                         eigvects.T)  # Calculo de C^{1/2}
            Cm12 = np.dot(np.dot(eigvects, np.diag(isqrt(eigvals))),
                          eigvects.T)  # Calculo de C^{-1/2}
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                #print("Eigenvalores matriz {ind}: {emb}".format(ind=index,emb=scipy.linalg.eigh(covmats[index])[0]))
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]),
                             Cm12)  #C^{-1/2}C_iC^{-1/2}
                eigvals, eigvects = scipy.linalg.eigh(tmp)
                logm = np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                              eigvects.T)  #log(C^{-1/2}C_iC^{-1/2})
                J += logm  # \sum log(C^{-1/2}C_iC^{-1/2})
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            lista_aproximaciones.append(crit)
            if crit < tol and flag == False:
                it_de_salida = k
                flag = True
            eigvals, eigvects = scipy.linalg.eigh(J, check_finite=False)
            C = np.dot(
                np.dot(
                    C12,
                    np.dot(np.dot(eigvects, np.diag(np.exp(eigvals))),
                           eigvects.T)),
                C12)  # C^{1/2}exp(\sum log(C^{-1/2}C_iC^{-1/2}))C^{1/2}
            C = 0.5 * (C + C.T)  #Symmetrization
        if itersTaken == True:
            return np.array(C), lista_aproximaciones, it_de_salida
        else:
            return np.array(C), lista_aproximaciones
    return


def mean_fletcher(covmats,
                  tol=10e-9,
                  maxiter=50,
                  init='mean',
                  norm='egvals',
                  itersTaken=False,
                  norm_approximation=False,
                  imprime=False):
    """
    Compute the Fletcher's mean algorithm to calculate the geometric mean of a list of SPD matrices.
    
    Parameters
    ----------
        covmats:
                    list.
                    List of SPD matrices.
                    
        tol:
                    float.
                    tolerance to stop the algorithm.
                    
        maxiter:
                    int.
                    Maximum number of iterations of the algorithm.
                    
        init:
                    array.
                    Base matrix to start the algoritm. Default start with ''mean`` (arithmetic mean).
                    Others: ''mean``, ''identity``, ''first``, ''log_euclidean``, or SPD matrix array.
                    
        norm:
                    str.
                    Type of norm to use. Default: ''egvals`` (sum of square of eigenvalues).
                    Other: ''frobenius``.
                    
        iterTaken:
                    bool.
                    True, if want to know No. Iterations needed by the algorithm.
                    
        norm_approximation:
                    bool.
                    True, to return a list with history of the Norm value Through the algorithm.
                    
        imprime:
                    bool.
                    True, to print information through the code.
     
    Returns
    -------
        C:
                    Array.
                    Fletcher's mean of covmats.
        it_de_salida (optional):
                    int.
                    No. iterations taken to pass the tolerance for first time.
                    
        lista_aproximaciones (optional):
                    list.
                    All approximations norms from 1 to maxiter.
                
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_fletcher(A)
            array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
           [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
           [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
           [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]])
        >>> mean_fletcher(A,itersTaken=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             8)
        >>> mean_fletcher(A,itersTaken=True, norm_approximation=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             [1.2499134146335775,
              0.038028555697487254,
              0.005157315385987814,
              0.0005183919740273995,
                  .
                  .
                  .
              2.5716369454988992e-14,
              2.6616446248399277e-14],
             8)   
    """
    covmats = regularization(covmats)
    Nt, Ne, Ne = covmats.shape
    #Choosing Initial Matrix
    if type(init) == str:
        if init == 'mean':
            C = np.mean(covmats, axis=0)
        if init == 'identity':
            C = np.identity(Ne)
        if init == 'first':
            C = covmats[0]
        if init == 'log_euclidean':
            C = mean_log_euclidean(covmats)[0]
    elif len(init.shape) == 2:
        C = init
    else:
        return print('No chosen Norm matrix')
    k = 0
    nu = 1.0
    tau = np.finfo(np.float64).max
    ll = [0, 0]  #Lista donde se van a ir guardando las normas
    isqrt = lambda x: 1. / np.sqrt(x)  #funcion que calcula x^{-1/2}
    if norm_approximation == False:
        while (tau > tol) and (k < maxiter):
            if imprime is True:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            eigvals, eigvects = scipy.linalg.eigh(
                C, check_finite=False)  #eigenvalues and eigenvectors of C
            C12 = np.dot(np.dot(eigvects, np.diag(np.sqrt(eigvals))),
                         eigvects.T)  #C^{1/2}
            Cm12 = np.dot(np.dot(eigvects, np.diag(isqrt(eigvals))),
                          eigvects.T)  #C^{-1/2}
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]),
                             Cm12)  #C^{-1/2} C_i C^{-1/2}
                eigvals, eigvects = scipy.linalg.eigh(tmp, check_finite=False)
                logm = np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                              eigvects.T)  #log(C^{-1/2} C_i C^{-1/2})
                J += logm  #\sum log(C^{-1/2}C_iC^{-1/2})
            J = np.dot(C12, np.dot(J, C12))
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            ll[0] = ll[1]
            ll[1] = crit
            tmp = nu * np.dot(Cm12, np.dot(J, Cm12))
            eigvals, eigvects = scipy.linalg.eigh(tmp, check_finite=False)
            expm_J = np.dot(np.dot(eigvects, np.diag(np.exp(eigvals))),
                            eigvects.T)
            C = np.dot(
                np.dot(C12, expm_J),
                C12)  #C^{1/2}exp(nu*\sum log(C^{-1/2}C_iC^{-1/2}))C^{1/2}
            C = 0.5 * (C + C.T)
            if k > 1 and ll[1] > ll[
                    0]:  #Si la norma aumenta el proximo \sum log(C^{-1/2}C_iC^{-1/2}) se multiplica por 0.5
                nu = 0.5 * nu
                tau = ll[0]  #Y se elige como parametro la norma anterior
            else:
                tau = ll[1]  #Si no, se toma esta última norma como parámetro
        it_de_salida = k
        if itersTaken == False:
            return np.array(C)
        else:
            return np.array(C), it_de_salida
    else:
        lista_aproximaciones = []
        flag = False
        while (k < maxiter):
            if imprime is True:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            eigvals, eigvects = scipy.linalg.eigh(
                C, check_finite=False)  #eigenvalues and eigenvectors of C
            C12 = np.dot(np.dot(eigvects, np.diag(np.sqrt(eigvals))),
                         eigvects.T)  #C^{1/2}
            Cm12 = np.dot(np.dot(eigvects, np.diag(isqrt(eigvals))),
                          eigvects.T)  #C^{-1/2}
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]),
                             Cm12)  #C^{-1/2} C_i C^{-1/2}
                eigvals, eigvects = scipy.linalg.eigh(tmp, check_finite=False)
                logm = np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                              eigvects.T)  #log(C^{-1/2} C_i C^{-1/2})
                J += logm  #\sum log(C^{-1/2}C_iC^{-1/2})
            J = np.dot(C12, np.dot(J, C12))
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            lista_aproximaciones.append(crit)
            ll[0] = ll[1]
            ll[1] = crit
            tmp = nu * np.dot(Cm12, np.dot(J, Cm12))
            eigvals, eigvects = scipy.linalg.eigh(tmp, check_finite=False)
            expm_J = np.dot(np.dot(eigvects, np.diag(np.exp(eigvals))),
                            eigvects.T)
            C = np.dot(
                np.dot(C12, expm_J),
                C12)  #C^{1/2}exp(nu*\sum log(C^{-1/2}C_iC^{-1/2}))C^{1/2}
            C = 0.5 * (C + C.T)
            if k > 1 and ll[1] > ll[
                    0]:  #Si la norma aumenta el proximo \sum log(C^{-1/2}C_iC^{-1/2}) se multiplica por 0.5
                nu = 0.5 * nu
                tau = ll[0]  #Y se elige como parametro la norma anterior
            else:
                tau = ll[1]  #Si no, se toma esta última norma como parámetro
            if crit < tol and flag == False:
                it_de_salida = k
                flag = True
        if itersTaken == True:
            return np.array(C), lista_aproximaciones, it_de_salida
        else:
            return np.array(C), lista_aproximaciones
    return


def mean_GE(covmats,
            tol=10e-9,
            maxiter=50,
            init='mean',
            norm='egvals',
            itersTaken=False,
            norm_approximation=False,
            imprime=False):
    """
    Calculate the geometric mean of a list of SPD matrices using Generalized Eigenvalues
    
    Parameters
    ----------
        covmats:
                    list.
                    List of SPD matrices.
                    
        tol:
                    float.
                    tolerance to stop the algorithm.
                    
        maxiter:
                    int.
                    Maximum number of iterations of the algorithm.
                    
        init:
                    array.
                    Base matrix to start the algoritm. Default start with ''mean`` (arithmetic mean).
                    Others: ''mean``, ''identity``, ''first``, ''log_euclidean``, or SPD matrix array.
                    
        norm:
                    str.
                    Type of norm to use. Default: ''egvals`` (sum of square of eigenvalues).
                    Other: ''frobenius``.
                    
        iterTaken:
                    bool.
                    True, if want to know No. Iterations needed by the algorithm.
                    
        norm_approximation:
                    bool.
                    True, to return a list with history of the Norm value Through the algorithm.
                    
        imprime:
                    bool.
                    True, to print information through the code.
     
    Returns
    -------
        C:
                    Array.
                    Geometric mean of covmats using generalized eigenvalues.
        it_de_salida (optional):
                    int.
                    No. iterations taken to pass the tolerance for first time.
                    
        lista_aproximaciones (optional):
                    list.
                    All approximations norms from 1 to maxiter.
                
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_GE(A)
            array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
           [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
           [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
           [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]])
        >>> mean_GE(A,itersTaken=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             8)
        >>> mean_GE(A,itersTaken=True, norm_approximation=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             [1.2499134146335775,
              0.038028555697487254,
              0.005157315385987814,
              0.0005183919740273995,
                  .
                  .
                  .
              2.5716369454988992e-14,
              2.6616446248399277e-14],
             8)  
    """
    covmats = regularization(covmats)
    Nt, Ne, Ne = covmats.shape
    #Choosing Initial Matrix
    if type(init) == str:
        if init == 'mean':
            C = np.mean(covmats, axis=0)
        if init == 'identity':
            C = np.identity(Ne)
        if init == 'first':
            C = covmats[0]
        if init == 'log_euclidean':
            C = mean_log_euclidean(covmats)[0]
    elif len(init.shape) == 2:
        C = init
    else:
        return print('No chosen Norm matrix')
    k = 0
    crit = np.finfo(np.float64).max
    if norm_approximation == False:
        #Start the mean algorithm
        while (crit > tol) and (k < maxiter):
            k = k + 1
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                #print("Eigenvalores matriz {ind}: {emb}".format(ind=index,emb=scipy.linalg.eigh(covmats[index])[0]))
                eigvals, eigvects = scipy.linalg.eigh(covmats[index], C)
                eigvals = np.diag(np.log(eigvals))
                J += (np.dot(np.dot(eigvects, eigvals),
                             eigvects.T))  #\sum log(Ci)
            J = (1 / Nt) * np.dot(np.dot(C, J), C)
            crit = matrix_norm(J, typeNorm=norm)
            eigvals, eigvects = scipy.linalg.eigh(J, C)
            eigvals = np.diag(np.exp(eigvals))
            C = np.dot(
                np.dot(np.dot(np.dot(C, eigvects), eigvals), eigvects.T), C)
            C = 0.5 * (C + C.T)
        it_de_salida = k
        if itersTaken == False:
            return np.array(C)
        else:
            return np.array(C), it_de_salida
    else:
        lista_aproximaciones = []
        flag = False
        while (k < maxiter):
            k = k + 1
            J = np.zeros((Ne, Ne))
            for index in range(Nt):
                eigvals, eigvects = scipy.linalg.eigh(covmats[index], C)
                eigvals = np.diag(np.log(eigvals))
                J += (np.dot(np.dot(eigvects, eigvals),
                             eigvects.T))  #\sum log(Ci)
            J = (1 / Nt) * np.dot(np.dot(C, J), C)
            crit = matrix_norm(J, typeNorm=norm)
            lista_aproximaciones.append(crit)
            eigvals, eigvects = scipy.linalg.eigh(J, C)
            eigvals = np.diag(np.exp(eigvals))
            C = np.dot(
                np.dot(np.dot(np.dot(C, eigvects), eigvals), eigvects.T), C)
            C = 0.5 * (C + C.T)
            if crit < tol and flag == False:
                it_de_salida = k
                flag = True
            C = 0.5 * (C + C.T)
        if itersTaken == True:
            return np.array(C), lista_aproximaciones, it_de_salida
        else:
            return np.array(C), lista_aproximaciones
    return


def mean_CHOL(covmats,
              tol=10e-9,
              maxiter=50,
              init='mean',
              norm='egvals',
              itersTaken=False,
              norm_approximation=False,
              imprime=False):
    """
    Calculate the geometric mean of a list of SPD matrices using a Cholesky Descomposition.
    
    Parameters
    ----------
        covmats:
                    list.
                    List of SPD matrices.
                    
        tol:
                    float.
                    tolerance to stop the algorithm.
                    
        maxiter:
                    int.
                    Maximum number of iterations of the algorithm.
                    
        init:
                    array.
                    Base matrix to start the algoritm. Default start with ''mean`` (arithmetic mean).
                    Others: ''mean``, ''identity``, ''first``, ''log_euclidean``, or SPD matrix array.
                    
        norm:
                    str.
                    Type of norm to use. Default: ''egvals`` (sum of square of eigenvalues).
                    Other: ''frobenius``.
                    
        iterTaken:
                    bool.
                    True, if want to know No. Iterations needed by the algorithm.
                    
        norm_approximation:
                    bool.
                    True, to return a list with history of the Norm value Through the algorithm.
                    
        imprime:
                    bool.
                    True, to print information through the code.
     
    Returns
    -------
        C:
                    Array.
                    Geometric mean of covmats using a cholesky descomposition.
        it_de_salida (optional):
                    int.
                    No. iterations taken to pass the tolerance for first time.
                    
        lista_aproximaciones (optional):
                    list.
                    All approximations norms from 1 to maxiter.
                
    Examples
    --------
        >>> A = genera_spd(20,4)
        >>> mean_CHOL(A)
            array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
           [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
           [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
           [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]])
        >>> mean_CHOL(A,itersTaken=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             8)
        >>> mean_CHOL(A,itersTaken=True, norm_approximation=True)
            (array([[ 0.68376135, -0.30235902, -0.08842775, -0.1294758 ],
            [-0.30235902,  1.04471373,  0.09702631,  0.35462957],
            [-0.08842775,  0.09702631,  0.80311548,  0.17108557],
            [-0.1294758 ,  0.35462957,  0.17108557,  0.74810202]]),
             [1.2499134146335775,
              0.038028555697487254,
              0.005157315385987814,
              0.0005183919740273995,
                  .
                  .
                  .
              2.5716369454988992e-14,
              2.6616446248399277e-14],
             8)  
    """
    covmats = regularization(covmats)
    Nt, Ne, Ne = covmats.shape
    #Choosing Initial Matrix
    if type(init) == str:
        if init == 'mean':
            C = np.mean(covmats, axis=0)
        if init == 'identity':
            C = np.identity(Ne)
        if init == 'first':
            C = covmats[0]
        if init == 'log_euclidean':
            C = mean_log_euclidean(covmats)[0]
    elif len(init.shape) == 2:
        C = init
    else:
        return print('No chosen Norm matrix')
    k = 0
    #print("Matriz Base en iteracion {kk}:\n {mb}".format(kk=k,mb=C))
    #print("Eigenvalores matriz base:\n {emb}".format(emb=scipy.linalg.eigh(C)[0]))
    crit = np.finfo(np.float64).max
    if norm_approximation == False:
        while (crit > tol) and (k < maxiter):
            if imprime is True:
                print("Eigenvalores matriz base en iteracion {kk}:\n {emb}".
                      format(emb=scipy.linalg.eigh(C)[0], kk=k))
            k = k + 1
            J = np.zeros((Ne, Ne))
            R = scipy.linalg.cholesky(C)  #triangular superior de C
            RT = R.T  #R transpuesta
            Rm1 = np.linalg.solve(R, np.identity(Ne))  #R^{-1}
            RmT = Rm1.T  #R^{-T} como la trasnpuesta de R^{-1}
            for index in range(Nt):
                L = np.dot(np.dot(RmT, covmats[index]),
                           Rm1)  #Calculo de R^{T} C_i R^{-1}
                eigvals, eigvects = scipy.linalg.eigh(L)
                J += (np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                             eigvects.T)
                      )  #Calculo de \sum log(R^{T} C_i R^{-1})
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            eigvals, eigvects = scipy.linalg.eigh(J, check_finite=False)
            C = np.dot(
                np.dot(np.dot(np.dot(RT, eigvects), np.diag(np.exp(eigvals))),
                       eigvects.T),
                R)  #Calculo de R^T exp(\sum log(R^{T} C_i R^{-1}))R
            C = 0.5 * (C + C.T)
        it_de_salida = k
        if itersTaken == False:
            return np.array(C)
        else:
            return np.array(C), it_de_salida
    else:
        lista_aproximaciones = []
        flag = False
        it_de_salida = maxiter
        while (k < maxiter):
            k = k + 1
            J = np.zeros((Ne, Ne))
            R = scipy.linalg.cholesky(C)  #triangular superior
            RT = R.T
            Rm1 = np.linalg.solve(R, np.identity(Ne))  #R^{-1}
            RmT = Rm1.T  #R^{-T}
            for index in range(Nt):
                Ci = np.dot(np.dot(RmT, covmats[index]), Rm1)
                eigvals, eigvects = scipy.linalg.eigh(Ci)
                J += (np.dot(np.dot(eigvects, np.diag(np.log(eigvals))),
                             eigvects.T))
            J = (1 / Nt) * J
            crit = matrix_norm(J, typeNorm=norm)
            lista_aproximaciones.append(crit)
            eigvals, eigvects = scipy.linalg.eigh(J, check_finite=False)
            C = np.dot(
                np.dot(np.dot(np.dot(RT, eigvects), np.diag(np.exp(eigvals))),
                       eigvects.T), R)
            C = 0.5 * (C + C.T)
            if crit < tol and flag == False:
                it_de_salida = k
                flag = True
        if itersTaken == True:
            return np.array(C), lista_aproximaciones, it_de_salida
        else:
            return np.array(C), lista_aproximaciones
    return
