# Parkinsonian Gait Patterns Quantification From Principal Geodesic Analysis

This repository is related to the paper entitle:

*"Parkinsonian gait patterns quantification from principal geodesic analysis."* <br/>

Santiago Niño <sup>1</sup>, Juan Olmos <sup>1</sup>, Juan C. Galvis <sup>2</sup>, Fabio Martínez <sup>1*</sup>.


<sup>1 </sup> Biomedical Imaging, Vision and Learning Laboratory ([BIVL$^2$ab](https://bivl2ab.uis.edu.co/)), Universidad Industrial de Santander (UIS), Bucaramanga 680002,Colombia.
<sup>2 </sup> Departamento de Matemáticas, Universidad Nacional de Colom-
bia, Bogotá D.C., Colombia. <br/>

<div align="center">
  <img src="imgs/pipeline.png" width="100%" height="70%"/>
</div><br/>

## Citation

For any work based on the proposed method or that uses the raw videos or the same covariance matrices, please cite the following article: 
    
[Niño, S., Olmos, J. A., Galvis, J. C., & Martínez, F., "Riemannian SPD learning to represent and characterize fixational oculomotor Parkinsonian abnormalities". Pattern Analysis and Applications (2023).](https://doi.org/10.1007/s10044-022-01115-x)

    @article{nino2023pga,
      title={Parkinsonian gait patterns quantification from principal geodesic analysis},
      author={Niño, Santiago and Olmos, Juan A and Galvis, Juan C and Martínez, Fabio},
      journal={Pattern Analysis and Applications},
      volume={26},
      number={2},
      pages={679--689},
      year={2023},
    }
<!--  note={Under revision} -->

# Data

The covariance matrices used in this work are located in the ``covs_DN20`` and ``covs_MN14`` folders. To download the raw video data: [Request Access to Dataset](mailto:famarcar@saber.uis.edu.co?subject=Request%20for%20access:%20PD%20gAIT%20Dataset). The **PD Gait Dataset** includes 11 patients diagnosed with PD and 11 Control subjects. The study incorporates PD subjects with different disease degree progression (Hoehn-Yahr rating scale). A total of 176 videos were recorded, having 8 samples per person, with a duration of approximately 4 seconds.

# Code
The main code is presented in the ``PARKINSON.ipynb notebook``. 

# Contact Information
For any doubt about the code and/or the dataset please contact:
* Juan A. Olmos: jaolmosr@correo.uis.edu.co
* Fabio Martínez: famarcar@saber.uis.edu.co

